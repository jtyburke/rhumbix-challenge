import logging

from flask import (
		flash, 
		Flask, 
		render_template, 
		request, 
		redirect, 
		url_for
)
import requests


app = Flask(__name__)
app.config['SECRET_KEY'] = "secret!" # needed for message flashing


@app.route('/', methods=['GET', 'POST'])
def index():
	REQUIRED_FIELDS = ['keyword']
	embed_url = "http://via.placeholder.com/350x150"
	if request.method == 'POST':
		if any(request.form.get(k) == "" for k in REQUIRED_FIELDS):
			flash("You have to fill out the form!")
			return redirect(url_for('index'))
		keyword = request.form['keyword']
		giphy_url = ('http://api.giphy.com/v1/gifs/search?q='
				'%s&api_key=dc6zaTOxFJmzC&limit=1' % keyword)
		r = requests.get(giphy_url)
		if r.status_code == 200:
			if r.json()['data']:
				imgs = r.json()['data'][0]['images']
				embed_url = imgs['downsized_medium']['url']
				flash("Showing results for %s" % keyword)
			else:
				flash("Weird: there weren't any results for %s!" % keyword)
		else:
			flash("Giphy returned a %s error: %s" % (
					r.status_code, r.json()['meta']['msg']))
	return render_template('index.html', embed_url=embed_url)


if __name__ == '__main__':
	app.run('0.0.0.0', port=5000, debug=True)
